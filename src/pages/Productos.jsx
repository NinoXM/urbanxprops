import React from 'react'

const Productos = () => {
  return (
<section id="productos" className="text-gray-600 body-font -mt-28">
  <div className="container px-5 py-24 mx-auto">
    <div className="flex flex-col text-center w-full ">
      <h1 className="text-2xl font-medium title-font mb-4 text-gray-900 tracking-widest">Lo Mas Nuevo</h1>
      <p className="lg:w-2/3 mx-auto leading-relaxed text-base mb-8">Crea el peinado de tus sueños usando nuestro ondas durag, modelos aptos para todo tipo de cabello para apoyarte durante este largo proceso.</p>
    </div>
    <div className="flex flex-wrap -m-4">
      <div className="p-4 lg:w-1/2">
        <div className="h-full flex sm:flex-row flex-col items-center sm:justify-start justify-center text-center sm:text-left">
          <img alt="team" className="flex-shrink-0 rounded-lg w-48 h-48 object-cover object-center sm:mb-0 mb-4" src="/src/assets/modelosGorras/1.png"/>
          <div className="flex-grow sm:pl-8">
            <h2 className="title-font font-medium text-lg text-gray-900">Durags Clasícos</h2>
            <h3 className="text-gray-500 mb-3">Colores negro y blanco</h3>
            <p className="mb-4">El clásico durag negro y blanco le permitirá dejar un toque personal extra a su estilo, dejando espacio para su imaginación y creatividad.</p>
            <a className="ml-2 text-gray-500" href='https://api.whatsapp.com/send?phone=584129369532&text=(Urban%20Xprops)%20Hey%20que%20tal%20vienes%20por%20%0AFlow%3F%20%0A%0A%0ACuentame..%20'
               target='_blank'>
            <span className="inline-flex">
             
                <svg fill="none" stroke="currentColor" strokeLinecap="round" strokeLinejoin="round" strokeWidth="2" className="w-5 h-5" viewBox="0 0 24 24">
                  <path d="M21 11.5a8.38 8.38 0 01-.9 3.8 8.5 8.5 0 01-7.6 4.7 8.38 8.38 0 01-3.8-.9L3 21l1.9-5.7a8.38 8.38 0 01-.9-3.8 8.5 8.5 0 014.7-7.6 8.38 8.38 0 013.8-.9h.5a8.48 8.48 0 018 8v.5z"></path>
                </svg>
              <span> Ordena Ahora!</span>
            </span>
            </a> 
          </div>
        </div>
      </div>
      <div className="p-4 lg:w-1/2">
        <div className="h-full flex sm:flex-row flex-col items-center sm:justify-start justify-center text-center sm:text-left">
          <img alt="team" className="flex-shrink-0 rounded-lg w-48 h-48 object-cover object-center sm:mb-0 mb-4" src="/src/assets/modelosGorras/2.png"/>
          <div className="flex-grow sm:pl-8">
            <h2 className="title-font font-medium text-lg text-gray-900">Durags Clasícos</h2>
            <h3 className="text-gray-500 mb-3">Colores rojo y azul</h3>
            <p className="mb-4">Un durag rojo y azul que es agradable y fácil de llevar para añadir un toque extra a tu aspecto diario</p>
            <a className="ml-2 text-gray-500" href='https://api.whatsapp.com/send?phone=584129369532&text=(Urban%20Xprops)%20Hey%20que%20tal%20vienes%20por%20%0AFlow%3F%20%0A%0A%0ACuentame..%20'
               target='_blank'>
            <span className="inline-flex">
              
                <svg fill="none" stroke="currentColor" strokeLinecap="round" strokeLinejoin="round" strokeWidth="2" className="w-5 h-5" viewBox="0 0 24 24">
                  <path d="M21 11.5a8.38 8.38 0 01-.9 3.8 8.5 8.5 0 01-7.6 4.7 8.38 8.38 0 01-3.8-.9L3 21l1.9-5.7a8.38 8.38 0 01-.9-3.8 8.5 8.5 0 014.7-7.6 8.38 8.38 0 013.8-.9h.5a8.48 8.48 0 018 8v.5z"></path>
                </svg>
              <span> Ordena Ahora!</span>
            </span>
            </a> 
          </div>
        </div>
      </div>
      <div className="p-4 lg:w-1/2">
        <div className="h-full flex sm:flex-row flex-col items-center sm:justify-start justify-center text-center sm:text-left">
          <img alt="team" className="flex-shrink-0 rounded-lg w-48 h-48 object-cover object-center sm:mb-0 mb-4" src="/src/assets/modelosGorras/3.png"/>
          <div className="flex-grow sm:pl-8">
            <h2 className="title-font font-medium text-lg text-gray-900">Durags Terciopelo</h2>
            <h3 className="text-gray-500 mb-3">Colores Negro y blanco</h3>
            <p className="mb-4">El indiscutible Durag negro y blanco en terciopelo, tanto apreciado por su gran comodidad como su ligereza, Ofreciéndole muchas posibilidades de peinado.</p>
            <a className="ml-2 text-gray-500" href='https://api.whatsapp.com/send?phone=584129369532&text=(Urban%20Xprops)%20Hey%20que%20tal%20vienes%20por%20%0AFlow%3F%20%0A%0A%0ACuentame..%20'
               target='_blank'>
            <span className="inline-flex">
             
                <svg fill="none" stroke="currentColor" strokeLinecap="round" strokeLinejoin="round" strokeWidth="2" className="w-5 h-5" viewBox="0 0 24 24">
                  <path d="M21 11.5a8.38 8.38 0 01-.9 3.8 8.5 8.5 0 01-7.6 4.7 8.38 8.38 0 01-3.8-.9L3 21l1.9-5.7a8.38 8.38 0 01-.9-3.8 8.5 8.5 0 014.7-7.6 8.38 8.38 0 013.8-.9h.5a8.48 8.48 0 018 8v.5z"></path>
                </svg>
              <span> Ordena Ahora!</span> 
            </span>
            </a>
          </div>
        </div>
      </div>
      <div className="p-4 lg:w-1/2">
        <div className="h-full flex sm:flex-row flex-col items-center sm:justify-start justify-center text-center sm:text-left">
          <img alt="team" className="flex-shrink-0 rounded-lg w-48 h-48 object-cover object-center sm:mb-0 mb-4" src="/src/assets/modelosGorras/4.png"/>
          <div className="flex-grow sm:pl-8">
            <h2 className="title-font font-medium text-lg text-gray-900">Variedad Clasicos</h2>
            <h3 className="text-gray-500 mb-3">Colores negro, blanco, rojo y azul</h3>
            <p className="mb-4">Un juego de cuatro durag directamente de nuestra colección clásica que te permitirá tener una multitud de opciones de color para combinar con tus trajes para que siempre tengas el que mejorará tu estilo de ropa y peinado con las innumerables características de este increíble accesorio.</p>
            <a className="ml-2 text-gray-500" href='https://api.whatsapp.com/send?phone=584129369532&text=(Urban%20Xprops)%20Hey%20que%20tal%20vienes%20por%20%0AFlow%3F%20%0A%0A%0ACuentame..%20'
               target='_blank'>
            <span className="inline-flex">
             
                <svg fill="none" stroke="currentColor" strokeLinecap="round" strokeLinejoin="round" strokeWidth="2" className="w-10 h-5" viewBox="0 0 24 24">
                  <path d="M21 11.5a8.38 8.38 0 01-.9 3.8 8.5 8.5 0 01-7.6 4.7 8.38 8.38 0 01-3.8-.9L3 21l1.9-5.7a8.38 8.38 0 01-.9-3.8 8.5 8.5 0 014.7-7.6 8.38 8.38 0 013.8-.9h.5a8.48 8.48 0 018 8v.5z"></path>
                </svg>
              <span> Ordena Ahora!</span>
            </span>
            </a> 
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
  )
}

export default Productos
