import React, { useState } from 'react'
import { Link } from 'react-scroll';
const NavBar = () => {
  const [hovered, setHovered] = useState(false);

  return (
    <header className="fixed w-full z-10 text-white bg-gradient-to-b from-black to-white body-font -mb-20">
      <div className="container mx-auto flex flex-wrap p-5 flex-col md:flex-row items-center">
        <a className="flex title-font font-medium items-center text-white mb-4 md:mb-0">
          <img src="/src/assets/img/urban.png" className="w-20 h-20 mb-2 text-white  rounded-full" alt="" />
          <span className="ml-3 text-6xl">
            <div onMouseOver={() => setHovered(true)} onMouseOut={() => setHovered(false)}>
              {hovered ? (
                <img src="/src/assets/img/logo1.png" className="max-w-80 max-w-100 h-15 text-white " alt="Descripción de la imagen hover" />
              ) : (
                <img src="/src/assets/img/logo.png" className="max-w-80 max-w-100 h-15 text-white " alt="Descripción de la imagen normal" />
              )}
            </div>
          </span>
        </a>
        <nav className="md:ml-auto md:mr-auto flex flex-wrap items-center text-base justify-center">
          <Link
            activeClass="active"
            to="header"
            spy={true}
            smooth={true}
            offset={-70}
            duration={500}
            className="mr-5 hover:text-orange-600 cursor-pointer"
          >
            Inicio
          </Link>

          <Link
            activeClass="active"
            to="productos"
            spy={true}
            smooth={true}
            offset={-70}
            duration={500}
            className="mr-5 hover:text-orange-600 cursor-pointer" >
            Lo Más Nuevo
          </Link>

          <Link
            activeClass="active"
            to="footer"
            spy={true}
            smooth={true}
            offset={-70}
            duration={500}
            className="mr-5 hover:text-orange-600 cursor-pointer">
            Contáctanos
          </Link>
        </nav>

        <button className="inline-flex items-center bg-orange-500  text-black border-0 py-1 px-3 focus:outline-none hover:bg-black rounded hover:text-orange-500 mt-4 md:mt-0">
          <a href="https://www.instagram.com/urbanxprop/" target='_blank'>Ir a Instagram</a>
          <svg fill="none" stroke="currentColor" strokeLinecap="round" strokeLinejoin="round" strokeWidth="2" className="w-4 h-4 ml-1" viewBox="0 0 24 24">
            <path d="M5 12h14M12 5l7 7-7 7"></path>
          </svg>
        </button>
      </div>
    </header>
  )
}

export default NavBar
