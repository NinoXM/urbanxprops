import React from 'react'
import NavBar from './NavBar'

const Header = () => {
    return (
        <section id="header" className="text-gray-600 body-font">
            <NavBar />
            <div className="container mx-auto flex px-5 py-24 md:flex-row flex-col items-center">
                <div className="lg:flex-grow lg:w-4/4 pr-8 md:w-3/4 pr-1 flex flex-col md:items-start md:text-left mb-16 md:mb-0 items-center text-center">
                    <h1 className="title-font sm:text-4xl text-3xl mb-2 font-medium text-gray-900">
                        Tienda | Gorras | Durags | Accesorios
                    </h1>
                    <p className="mb-8 leading-relaxed w-96">Aprovecha estos modelos únicos y limitados para que lleves tu outfit al siguiente nivel con este Modelo Exclusivo y Original tanto en gorras como en Waves Cap Durags  .</p>
                    <div className="flex justify-center">

                        <a href="https://www.instagram.com/direct/t/110916186972286/"
                            className="inline-flex text-black bg-orange-500 
                                border-0 py-2 px-6 focus:outline-none
                                hover:bg-black hover:text-orange-500 rounded text-lg">
                            Chatea con Urban
                        </a>


                    </div>
                </div>

                <div className="flex flex-wrap w-1/2">
                    <div className="md:p-2 p-1 w-full">
                        <img alt="gallery" className="w-full h-full object-cover object-center block" src="/src/assets/modelosGorras/13.png" />
                    </div>
                    <div className="md:p-2 p-1 w-1/2">
                        <img alt="gallery" className="w-full object-cover h-full object-center block" src="/src/assets/modelosGorras/28.png" />
                    </div>
                    <div className="md:p-2 p-1 w-1/2">
                        <img alt="gallery" className="w-full object-cover h-full object-center block" src="/src/assets/modelosGorras/27.png" />
                    </div>
                </div>

                <div className="flex flex-wrap w-1/2">
                    <div className="md:p-2 p-1 w-1/2">
                        <img alt="gallery" className="w-full object-cover h-full object-center block" src="/src/assets/modelosGorras/51.png" />
                    </div>
                    <div className="md:p-2 p-1 w-1/2">
                        <img alt="gallery" className="w-full object-cover h-full object-center block" src="/src/assets/modelosGorras/59.png" />
                    </div>
                    <div className="md:p-2 p-1 w-full">
                        <img alt="gallery" className="w-full h-full object-cover object-center block" src="/src/assets/modelosGorras/57.png" />
                    </div>


                </div>
            </div>

            <div className="container px-5 py-24 mx-auto -mt-40">
                <div className="flex flex-wrap -m-4">
                    <div className="xl:w-1/4 md:w-1/2 p-4">
                        <div className="bg-gray-100 p-6 rounded-lg">
                            <img className="h-40 rounded w-full object-cover object-center mb-6" src="/src/assets/modelosGorras/40.png" alt="content" />
                            <h3 className="tracking-widest text-indigo-500 text-xs font-medium title-font">Disponible ✅</h3>
                            <h2 className="text-lg text-gray-900 font-medium title-font mb-4">🔥Estilo urbano y fresco🔥</h2>
                            <p><a href="https://www.instagram.com/p/CxA8pJ8uxlb/" target="_black" className='items-center text-center'>
                            📝Ordena Aqui!
                            </a>
                            </p>

                        </div>
                    </div>
                    <div className="xl:w-1/4 md:w-1/2 p-4">
                        <div className="bg-gray-100 p-6 rounded-lg">
                            <img className="h-40 rounded w-full object-cover object-center mb-6" src="/src/assets/modelosGorras/56.png" alt="content" />
                            <h3 className="tracking-widest text-indigo-500 text-xs font-medium title-font">Disponible ✅</h3>
                            <h2 className="text-lg text-gray-900 font-medium title-font mb-4">Destaca tu personalidad🔥</h2>
                                <p><a href="https://www.instagram.com/p/Cw_V5J5MjY0/" target="_black" className='items-center text-center'>
                            📝Ordena Aqui!
                            </a>
                            </p>
                        </div>
                    </div>
                    <div className="xl:w-1/4 md:w-1/2 p-4">
                        <div className="bg-gray-100 p-6 rounded-lg">
                            <img className="h-40 rounded w-full object-cover object-center mb-6" src="/src/assets/modelosGorras/49.png" alt="content" />
                            <h3 className="tracking-widest text-indigo-500 text-xs font-medium title-font">Disponible ✅</h3>
                            <h2 className="text-lg text-gray-900 font-medium title-font mb-4">🔥¡No la quieres, LA NECESITAS! 🔥
                            </h2>
                            <p><a href="https://www.instagram.com/p/Cw04_u6uETv/" target="_black" className='items-center text-center'>
                            📝Ordena Aqui!
                            </a>
                            </p>
                        </div>
                    </div>
                    <div className="xl:w-1/4 md:w-1/2 p-4">
                        <div className="bg-gray-100 p-6 rounded-lg">
                            <img className="h-40 rounded w-full object-cover object-center mb-6" src="/src/assets/modelosGorras/50.png" alt="content" />
                            <h3 className="tracking-widest text-indigo-500 text-xs font-medium title-font">Disponible ✅</h3>
                            <h2 className="text-lg text-gray-900 font-medium title-font mb-4">🔥El flow permanece pero la moda Pasa 🔥
                            </h2>
                            <p><a href="https://www.instagram.com/p/Cw-TRiJum_e/" target="_black" className='items-center text-center'>
                            📝Ordena Aqui!
                            </a>
                            </p>
                        </div>
                    </div>
                </div>
            </div>







        </section>
    )
}

export default Header