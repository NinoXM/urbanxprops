import Footer from "./components/Footer"
import Header from "./components/Header"
import Productos from "./pages/Productos"
import { Link } from 'react-router-dom';

function App() {

  return (
    <>
    

      <div className="bg-white min-h-screen">
      <Header ></Header>
      <Productos ></Productos>
      <Footer ></Footer>
      </div>

    </>
  )
}

export default App
